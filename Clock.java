import java.text.SimpleDateFormat;
import java.util.Date;

public class Clock {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
    //Method to continously update and print the current time
    public static void updateAndPrintTime() {
        while(true){
            Date currentTime = new Date();
            System.out.println(dateFormat.format(currentTime));
            try {
                Thread.sleep(1000); //Update every second
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args){
        //Create a thread for printing the time to the console
        Thread displayThread = new Thread(Clock::updateAndPrintTime);
        displayThread.setPriority(Thread.MAX_PRIORITY); //Highest priority for display thread
        //Create a separate thread for background updating
        Thread updateThread = new Thread(() -> {
            while(true){
                //Background update logic can be added here if needed
                try {
                    Thread.sleep(1000); //Update every second
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        updateThread.setPriority(Thread.MIN_PRIORITY); //Lower priority for background updating thread
        //Start both threads
        displayThread.start();
        updateThread.start();
    }
}